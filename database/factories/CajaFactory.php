<?php

namespace Database\Factories;

use App\Models\Caja;
use Illuminate\Database\Eloquent\Factories\Factory;

class CajaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Caja::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'denomination' => $this->faker->unique()->randomElement($array = array(.1, .5, 1, 2, 5, 10, 20, 50, 100, 200, 500)),
            'inventory' => $this->faker->numberBetween($min = 5, $max = 10),
        ];
    }
}
