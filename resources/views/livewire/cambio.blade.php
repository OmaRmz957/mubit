<div>
    
    <h4 class="mb-3 text-muted text-center">
        Cambio a devolver:
    </h4>
    <h4 class="mb-3 text-muted text-center">
        $ {{ $cambio }}
    </h4>
    
    @if(!empty($changes[0])) 
        <div class="row">
            @foreach($changes as $change)

                <div class="col-sm-3">
                    <div class="card my-2">
                        <div class="card-header text-center font-weight-bold">
                            Opcion {{ $loop->iteration }}
                        </div>

                        <ul class="list-group list-group-flush">
                            @foreach($change as $key => $value)
                                @if ($value != 0)
                                <li class="list-group-item">
                                    {{ $inventory[$key]['denomination'] >= 20 ? 'Billete(s)' : 'Moneda(s)' }} 
                                    $ {{ $inventory[$key]['denomination'] }}: {{ $value }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                
            @endforeach
        </div>
    @endif

    <button class="btn btn-success btn-lg btn-block" wire:click.prevent="confirm">
        Confirmar
    </button>

</div>
