<div>
    
    <div class="col-md-8 mx-auto">

        <div class="row">
            <div class="col-md-6 mb-3 mx-auto">
                <h4 class="mb-3">Monto total de la venta</h4>

                <input type="text" class="form-control" wire:model.lazy="total" placeholder="" required>
                @error('total')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 mb-3 mx-auto">
                <h4 class="mb-3">Monto recibido por el cliente</h4>

                <input type="text" class="form-control" wire:model.lazy="dinero_recibido" placeholder="" required>
                @error('dinero_recibido')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        
        <button class="btn btn-primary btn-lg btn-block" wire:click.prevent="change">
            Pagar
        </button>
    
    </div>
    
    @if($changes)
        
        <hr class="mb-4">

        <div class="col-md-8 mx-auto">

            <livewire:cambio :changes="$changes"/>
        
        </div>

    @endif

</div>
