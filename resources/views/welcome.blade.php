@extends('layouts.app')

@section('css')
    <style>
        .row {
        display: flex;
        flex-wrap: wrap;
        }

        .row > div[class*='col-'] {
        display: flex;
        }
    </style>
@endsection

@section('content')
    
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Ventas</h1>
        <p class="lead">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam deleniti itaque dolorum molestiae ipsa, neque odio. Quam tempora, ipsa, illum laboriosam unde, quis placeat optio iste modi fuga assumenda! Accusamus.
        </p>
    </div>

    <hr class="mb-4">

    <livewire:caja />

@endsection