<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        @include('partials.css')

    </head>

    <body>    
        
        <div class="container">
            
            @include('partials.header')

            @yield('content')
        
            @include('partials.footer')
            
            @include('partials.js')

        </div>
        
    </body>
</html>
