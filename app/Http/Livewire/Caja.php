<?php

namespace App\Http\Livewire;

use App\Models\Caja as ModelsCaja;
use Livewire\Component;

class Caja extends Component
{
    public $dinero_recibido, $total, $cambio;

    public $monedas, $inventory, $changes;

    protected $rules = [
        'total' => ['required', 'numeric', 'gt:0'],
        'dinero_recibido' => ['required', 'numeric', 'gt:0', 'gte:total']
    ];

    protected $messages = [
        'total.required' => 'Ingresa un monto de venta',
        'total.numeric' => 'Ingresa un monto de venta con valor numerico',
        'total.gt:0' => 'Ingresa un monto de venta mayor a 0',

        'dinero_recibido.required' => 'Ingresa un monto de pago',
        'dinero_recibido.numeric' => 'Ingresa un monto de pago con valor numerico',
        'dinero_recibido.gt:0' => 'Ingresa un monto de pago mayor a 0',
        'dinero_recibido.gte:0' => 'Ingresa un monto de pago mayor',
    ];

    protected $listeners = ['resetForm'];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function resetForm()
    {
        $this->reset();
    }


    public function change()
    {
        $this->validate();

        $this->cambio = $this->dinero_recibido - $this->total;

        $inventory = ModelsCaja::orderBy('denomination', 'ASC')->get();

        $this->monedas   = $inventory->pluck('denomination');

        $this->inventory = $inventory->pluck('inventory');

        $this->changes = $this->calcChange($this->monedas, $this->inventory, $this->cambio, $inventory);
        
        $this->emit('calculateChange', $this->monedas, $this->cambio, $this->changes, $inventory);
    }

    public function calcChange($coin_value, $inventory, $price, $invent) {
        $have = $invent->sum(function ($caja) {
            return $caja['denomination'] * $caja['inventory'];
        });
      
        $stack = [ [0, $price, $have, []] ];
        $best = collect();
      
        while (!empty($stack)) {
      
            // each stack call traverses a different set of parameters
            $parameters = array_pop($stack);
            $i = $parameters[0];
            $owed = $parameters[1];
            $have = $parameters[2];
            $result = $parameters[3];
      
            if ($owed <= 0) {
                if ($owed > 0) {
                    $best = [$owed, $result];
                } else if ($owed == 0) {
                    $best[] = $result;
                }
                continue;
            }
      
            // skip if we have none of this coin
            if ($inventory[$i] == 0) {
                $result[] = 0;
                $stack[] = [$i + 1, $owed, $have, $result];
                continue;
            }
      
            // minimum needed of this coin
            $need = $owed - $have + $inventory[$i] * $coin_value[$i];
      
            if ($need < 0) {
                $min = 0;
            } else {
                $min = ceil($need / $coin_value[$i]);
            }
      
            // add to stack
            for ($j = $min; $j <= $inventory[$i]; $j++) {
                $stack[] = [$i + 1, $owed - $j * $coin_value[$i], $have - $inventory[$i] * $coin_value[$i], array_merge($result,[$j])];
                if ($owed - $j * $coin_value[$i] < 0) {
                    break;
                }
            }
        }
        
        return $best;
    }

    public function render()
    {
        $changes = $this->changes;

        return view('livewire.caja', compact('changes'));
    }
}
