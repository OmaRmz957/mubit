<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Cambio extends Component
{
    protected $monedas, $cambio, $changes, $inventory;

    protected $listeners = ['calculateChange'];

    public function calculateChange($monedas, $cambio, $changes, $inventory)
    {
        $this->monedas = $monedas;
        $this->cambio = $cambio;
        $this->changes = collect($changes)->sortKeysDesc();
        $this->inventory = collect($inventory);
    }

    public function confirm()
    {
        $this->alert('success', 'Has pagado con exito!', [
            'position' =>  'center', 
            'timer' =>  3000,  
            'toast' =>  false, 
            'text' =>  'Genial', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);

        $this->emit('resetForm');

        $this->reset();
    }

    public function render()
    {
        $monedas = $this->monedas;
        $cambio  = $this->cambio;
        $changes = $this->changes;
        $inventory = $this->inventory;

        return view('livewire.cambio', compact('monedas', 'changes', 'cambio', 'inventory'));
    }
}
